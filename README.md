# kata Bank

- deposit 
- withdrawal
- account statement
- bonus: overdraft management

## Stack
- angular 13.2.1
- angular material 13.2.1
- angular CLI 13.2.2
- node 16.10.0
- scss
- karma
- parse (back4app to manage front-back exchange via API or directly via parse)

## Installation
- node 16.10.0
- angular CLI 13.2.2 
    - open a command line prompt and create a new repository by typing : _mkdir [name of your repository]_
    - cd [name of your repository]
    - npm install @angular/cli@13.2.2

## Startup
- open a command line prompt
- go to the base of the project > my-bank 
- type npm install
- ng serve
    - launch the app in your navigator

## User's connexion
There are two users available

_login_: user 1,
_password_: user1

_login_: user 2,
_password_: user2

## Author
Ben DA SILVEIRA
