import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { CustomerAreaComponent } from './components/customer-area/customer-area.component';
import { AccountStatementComponent } from './components/customer-area/account-statement/account-statement.component';
import { WithdrawalComponent } from './components/customer-area/withdrawal/withdrawal.component';
import { DepositComponent } from './components/customer-area/deposit/deposit.component';
import { AuthentificationGuardService } from './services/authentification-guard.service';
import { OffersComponent } from './components/offers/offers.component';
import { OpenAnAccountComponent } from './components/open-an-account/open-an-account.component';
import { AddAnAccountComponent } from './components/add-an-account/add-an-account.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';

const routes: Routes = [
  { path: '', redirectTo: 'mybank/offers', pathMatch: 'full' },
  {
    path: 'mybank',
    children: [
      { path: '', redirectTo: 'offers', pathMatch: 'full' },
      { path: 'offers', component: OffersComponent },
      { path: 'open-an-account', component: OpenAnAccountComponent },
      { path: 'add-an-account', component: AddAnAccountComponent},
      { path: 'connexion', component: ConnexionComponent },
      { path: 'contact-us', component: ContactUsComponent },
      { path: 'customer-area', component: CustomerAreaComponent,
        children: [
          { path: '', redirectTo: 'account-statement', pathMatch: 'full' },
          { path: 'account-statement', component: AccountStatementComponent, canActivate: [AuthentificationGuardService] },
          { path: 'withdraw', component: WithdrawalComponent, canActivate: [AuthentificationGuardService] },
          { path: 'deposit', component: DepositComponent, canActivate: [AuthentificationGuardService] }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthentificationGuardService]
})
export class AppRoutingModule { }
