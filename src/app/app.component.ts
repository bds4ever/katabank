import { Component } from '@angular/core';
import Parse from 'parse';
import { Router } from '@angular/router';
import { MatSnackBarService } from './services/mat-snack-bar.service';
import { SuperService } from './services/super.service';
import { MyObservableService } from './services/my-observable.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mybank';

  connectedUser: any;

  constructor(private superService: SuperService,
              private myObservableService: MyObservableService,
              private matSnackBarService: MatSnackBarService,
              private router: Router){

    this.myObservableService.dataSend$.subscribe((values: any)=>{
      if(values.cible === 'app-component'){
        this.getConnectedUser();
      }
    })

    this.getConnectedUser();
  }

  /**
   * @name getConnectedUser
   * @desc get connecter user
   */
  getConnectedUser(){
    this.superService.getConnectedUser().then((res: any)=>{
      this.connectedUser = res;
    })
  }

  /**
   * @name logOut
   * @desc log out from the app
   */
  logOut(){
    Parse.User.logOut().then((resp) => {
      this.matSnackBarService.openSnackBar(`Déconnexion réussie`, '', 'notif-success');
      // mettre à jour le header
      this.myObservableService.sendData('app-component', {});
      this.connectedUser = null;
      // rediriger vers la page principale
      this.router.navigate(['mybank/offers']);
      console.log('Logged out successfully', resp);
    }, err => {
      console.log('Error logging out', err);
    });
  }
}
