import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Parse } from "parse";

import { ConnexionComponent } from './components/connexion/connexion.component';
import { CustomerAreaComponent } from './components/customer-area/customer-area.component';
import { AccountStatementComponent } from './components/customer-area/account-statement/account-statement.component';
import { WithdrawalComponent } from './components/customer-area/withdrawal/withdrawal.component';
import { DepositComponent } from './components/customer-area/deposit/deposit.component';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarService } from './services/mat-snack-bar.service';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { OffersComponent } from './components/offers/offers.component';
import { OpenAnAccountComponent } from './components/open-an-account/open-an-account.component';
import { AddAnAccountComponent } from './components/add-an-account/add-an-account.component';
import { MatSortModule } from '@angular/material/sort';
import { ContactUsComponent } from './components/contact-us/contact-us.component';

/**
 * conf parse back4app
 */
Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
Parse.serverURL = environment.serverURL;

@NgModule({
  declarations: [
    AppComponent,
    ConnexionComponent,
    CustomerAreaComponent,
    AccountStatementComponent,
    WithdrawalComponent,
    DepositComponent,
    OffersComponent,
    OpenAnAccountComponent,
    AddAnAccountComponent,
    ContactUsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatTableModule,
    HttpClientModule,
    MatPaginatorModule,
    MatSortModule
  ],
  providers: [MatSnackBarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
