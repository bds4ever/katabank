import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAnAccountComponent } from './add-an-account.component';

describe('AddAnAccountComponent', () => {
  let component: AddAnAccountComponent;
  let fixture: ComponentFixture<AddAnAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAnAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAnAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
