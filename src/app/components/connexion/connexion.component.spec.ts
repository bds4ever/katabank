import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnexionComponent } from './connexion.component';
import { AppRoutingModule } from '../../app-routing.module';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';
import { SuperService } from 'src/app/services/super.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TestMock } from 'src/app/testUclass/test-mock';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ConnexionComponent', () => {
  let component: ConnexionComponent;
  let fixture: ComponentFixture<ConnexionComponent>;
  let superService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnexionComponent ],
      imports:[
        AppRoutingModule,
        HttpClientTestingModule,
        HttpClientModule,
        MatSnackBarModule,
        BrowserAnimationsModule
      ],
      providers:[
        MatSnackBarService,
        SuperService,
        MatSnackBarService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    superService = TestBed.inject(SuperService);
    fixture = TestBed.createComponent(ConnexionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should connect*', () => {
    component.login = 'user';
    component.password = 'user';
    component.connectedUser = TestMock.connectedUserMck;
    spyOn(superService, 'getConnectedUser').and.returnValue(Promise.resolve(TestMock.connectedUserMck));
    component.connexion();
    // expect(superService.getConnectedUser).toHaveBeenCalled();
  });
});
