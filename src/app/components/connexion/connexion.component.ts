import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Parse } from 'parse';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';
import { MyObservableService } from 'src/app/services/my-observable.service';
import { SuperService } from '../../services/super.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {

  hide = true;

  password: string;
  login: string;

  connectedUser: any;

  constructor(private router: Router,
    private superService: SuperService,
    private myObservableService: MyObservableService,
    private matSnackBarService: MatSnackBarService) { }

  ngOnInit(): void {
  }

  /**
   * @name connexion
   * @desc se connecter à l'espace client
   */
  async connexion() {
    try {
      let user: Parse.User = await Parse.User.logIn(this.login, this.password);
      // récupérer les données de l'utilisateur loggé
      this.connectedUser = await this.superService.getConnectedUser();
      if (this.connectedUser) {
        // affichage du message de bienvenue
        const message = `Hello` + ` ` + this.connectedUser.username;
        this.matSnackBarService.openSnackBar(message, '', 'notif-success');
        // rafraîchissement de la page principale (toolbar)
        this.myObservableService.sendData('app-component', {});
        this.router.navigateByUrl('/mybank/customer-area');
      }
    } catch (error: any) {
      const message = ``;
      this.matSnackBarService.openSnackBar(message, '', 'notif-alert');
      console.error('Error while signing up user', error);
    }
  }
}
