import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';
import { SuperService } from 'src/app/services/super.service';
import { TestMock } from 'src/app/testUclass/test-mock';

import { AccountStatementComponent } from './account-statement.component';

import {of} from 'rxjs';

const superServiceMock = {
  getConnectedUser() {
    return of(TestMock.connectedUserMck)
  },

  getPreviousBalance() {
    return of(10)
  }
};

describe('AccountStatementComponent', () => {
  let component: AccountStatementComponent;
  let fixture: ComponentFixture<AccountStatementComponent>;
  let superService: SuperService;

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [ AccountStatementComponent ],

      imports: [
        MatTableModule,
        MatSortModule,
        HttpClientModule,
        MatTooltipModule,
        AppRoutingModule,
        FormsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatFormFieldModule
      ],

      providers: [
        { provide: SuperService, useValue: superServiceMock},
        MatSnackBarService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    superService = TestBed.inject(SuperService)
    fixture = TestBed.createComponent(AccountStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get connected user', (() => {
    spyOn(superService, 'getConnectedUser').and.returnValue(Promise.resolve(TestMock.connectedUserMck));
    component.getConnectedUser();
    expect(superService.getConnectedUser).toHaveBeenCalled();
  }));

  it('should get the previous balance', (() => {
    spyOn(superService, 'getPreviousBalance').and.returnValue(Promise.resolve(10));
    component.getPreviousBalance();
    expect(superService.getPreviousBalance).toHaveBeenCalled();
  }));

});
