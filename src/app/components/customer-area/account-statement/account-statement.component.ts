import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';
import { MatSort, Sort } from '@angular/material/sort';
import { SuperService } from '../../../services/super.service';
import { ConnectedUserInterface } from 'src/app/interfaces/connected-user-interface';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { AngularCsv } from 'angular-csv-ext/dist/Angular-csv';

@Component({
  selector: 'app-account-statement',
  templateUrl: './account-statement.component.html',
  styleUrls: ['./account-statement.component.scss']
})
export class AccountStatementComponent implements OnInit {

  connectedUser: any;
  allBankOperationByUser: any;

  previousBalance: number;

  isBalanceReady: boolean = false;
  isAuthenticated: boolean = false;

  dataSource = new MatTableDataSource();

  displayedColumns: string[] = ['createdAt', 'deposit', 'withdrawal', 'balance'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('htmlData') htmlData: ElementRef;

  constructor(private superService: SuperService,
    private matSnackBarService: MatSnackBarService,
    private _liveAnnouncer: LiveAnnouncer) {
  }

  ngOnInit(): void {
    this.getConnectedUser().then(() => {
      this.getPreviousBalance();
      this.getAllBankOperationByUser().then(() => {
        this.dataSource.data = this.allBankOperationByUser;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    });
  }

  /**
   * @name getConnectedUser
   * @desc get connecter user
   */
  async getConnectedUser() {
    await this.superService.getConnectedUser().then((res: ConnectedUserInterface) => {
      this.connectedUser = res;
    })
  }

  /**
   * @name getPreviousBalance
   * @desc récuperer le solde de la dernière opération
   */
  async getPreviousBalance() {
    await this.superService.getPreviousBalance().then((resp: number) => {
      this.previousBalance = resp;
      this.isBalanceReady = true;
      return this.previousBalance;
    }, () => {
      const message = `Sorry, your balance is not available`;
      this.matSnackBarService.openSnackBar(message, '', 'notif-alert');
    })
  }

  /**
   * @name getAllBankOperation
   * @desc récupérer l'ensemble des opérations realisées par le user loggé
   * @param idConnectedUser
   */
  async getAllBankOperationByUser() {
    if (this.connectedUser) {
      await this.superService.getAllBankOperationByUser(this.connectedUser.objectId).then((resp: any) => {
        this.allBankOperationByUser = resp;
        console.log(this.allBankOperationByUser)
        return this.allBankOperationByUser;
      }, () => {
        console.log('erreur recup toutes operations')
      })
    }
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  /**
   * @name printAccountStatement
   * @desc télécharger le recap
   */
  async downloadAccountStatement() {
    let downloadData = [];
    this.allBankOperationByUser.forEach(el => {
      delete el.objectId;
      delete el.updatedAt;
      delete el.idUser;

      let d1 = new Date (el.createdAt);
      let d2 = d1.toDateString()

      let newValues = {
        'createdAt': d2,
        'deposit': el.deposit,
        'withdrawal': el.withdrawal,
        'balance': el.balance
      }

      downloadData.push(newValues);
    })

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Account statement of' + ` ` + this.connectedUser.username + ` ` + `-` + ` ` + 
              `Balance :` + ` ` + this.previousBalance + `€`,
      useBom: false,
      noDownload: false,
      headers: ["Date", "Deposit", "Withdrawal", "Account balance"],
      useHeader: false,
      nullToEmptyString: true,
    }

    new AngularCsv(downloadData, 'Account statement', options)
  }
}


