import { Component, OnInit } from '@angular/core';
import { SuperService } from '../../services/super.service';

@Component({
  selector: 'app-customer-area',
  templateUrl: './customer-area.component.html',
  styleUrls: ['./customer-area.component.scss']
})
export class CustomerAreaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
