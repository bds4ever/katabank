import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositComponent } from './deposit.component';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { SuperService } from '../../../services/super.service';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';

import { TestMock } from 'src/app/testUclass/test-mock';

import {of} from 'rxjs';

const superServiceMock = {
  getConnectedUser() {
    return of(TestMock.connectedUserMck)
  },

  getPreviousBalance() {
    return of(10)
  }
};

describe('DepositComponent', () => {
  let component: DepositComponent;
  let fixture: ComponentFixture<DepositComponent>;
  let superService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositComponent ],

      imports: [
        HttpClientTestingModule,
        HttpClientModule,
        MatInputModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        MatButtonModule,
      ],

      providers: [
        MatSnackBarService,
        { provide: SuperService, useValue: superServiceMock },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    superService = TestBed.inject(SuperService);
    fixture = TestBed.createComponent(DepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get connected user', (() => {
    spyOn(superService, 'getConnectedUser').and.returnValue(Promise.resolve(TestMock.connectedUserMck));
    component.getConnectedUser();
    expect(superService.getConnectedUser).toHaveBeenCalled();
  }));

  it('should get the previous balance', (() => {
    spyOn(superService, 'getPreviousBalance').and.returnValue(Promise.resolve(10));
    component.getPreviousBalance();
    expect(superService.getPreviousBalance).toHaveBeenCalled();
  }));

  // it('should deposit', (() => {
  //   spyOn(superService, 'getPreviousBalance').and.returnValue(Promise.resolve(10));

  //   component.connectedUser = TestMock.connectedUserMck;
  //   spyOn(superService, 'depositOrWithdraw').and.returnValue(of(TestMock.postResultMck));
  //   component.deposit();
  //   expect(superService.depositOrWithdraw).toHaveBeenCalled();
  // }));
});
