import { Component, OnInit } from '@angular/core';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';
import { MyObservableService } from '../../../services/my-observable.service';
import { SuperService } from '../../../services/super.service';

import { ConnectedUserInterface } from '../../../interfaces/connected-user-interface';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss']
})
export class DepositComponent implements OnInit {

  amount: number;
  previousBalance: number;

  isBalanceReady: boolean = false;

  connectedUser: any;

  constructor(private superService: SuperService,
    private matSnackBarService: MatSnackBarService) { }

  ngOnInit(): void {
    this.getConnectedUser();
    this.getPreviousBalance();
  }

  /**
   * @name getConnectedUser
   * @desc get connecter user
   */
  async getConnectedUser() {
    await this.superService.getConnectedUser().then((res: ConnectedUserInterface) => {
      this.connectedUser = res;
    })
  }

  /**
 * @name getPreviousBalance
 * @desc récuperer le solde de la dernière opération
 */
  async getPreviousBalance() {
    await this.superService.getPreviousBalance().then((resp: number) => {
      this.previousBalance = resp;
      this.isBalanceReady = true;
      return this.previousBalance;
    }, () => {
      const message = `Sorry, your balance is not available` +
        `Veuillez réessayer plus tard`;
      this.matSnackBarService.openSnackBar(message, '', 'notif-alert');
    })
  }

  /**
   * @name deposit
   * @desc déposer de l'argent sur son compte
   */
  async deposit() {
    // récuperer le solde de la dernière opération
    await this.superService.getPreviousBalance().then((resp: number) => {
      this.previousBalance = resp;
    })
    // nouvelle opération
    const dataToSave = {
      'typeOperation': 'deposit',
      'deposit': this.amount,
      'withdrawal': 0,
      'idUser': this.connectedUser.objectId,
      'balance': this.previousBalance + this.amount
    }
    // lancement de la sauvegarde
    this.superService.depositOrWithdraw(dataToSave).subscribe(() => {
      this.matSnackBarService.openSnackBar(`Success`, '', 'notif-success');
      // maj de la balance sur la page courante
      this.amount = 0;
      this.ngOnInit();
    }, error => {
      this.matSnackBarService.openSnackBar(error.error.error, '', 'notif-alert');
    })
  }
}
