import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawalComponent } from './withdrawal.component';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'src/app/app-routing.module';

import { TestMock } from 'src/app/testUclass/test-mock';
import {of} from 'rxjs';
import { SuperService } from 'src/app/services/super.service';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';


const superServiceMock = {
  getConnectedUser() {
    return of(TestMock.connectedUserMck)
  },

  getPreviousBalance() {
    return of(10)
  },

  depositOrWithdraw() {
    return of(TestMock.postResultMck)
  }
};

describe('WithdrawalComponent', () => {
  let component: WithdrawalComponent;
  let fixture: ComponentFixture<WithdrawalComponent>;
  let superService: SuperService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WithdrawalComponent ],

      imports: [
        HttpClientTestingModule,
        HttpClientModule,
        MatInputModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        MatButtonModule,
        AppRoutingModule
      ],

      providers: [
        MatSnackBarService,
        { provide: SuperService, useValue: superServiceMock }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    superService = TestBed.inject(SuperService);
    fixture = TestBed.createComponent(WithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get connected user', (() => {
    spyOn(superService, 'getConnectedUser').and.returnValue(Promise.resolve(TestMock.connectedUserMck));
    component.getConnectedUser();
    expect(superService.getConnectedUser).toHaveBeenCalled();
  }));

  it('should get the previous balance', (() => {
    spyOn(superService, 'getPreviousBalance').and.returnValue(Promise.resolve(10));
    component.getPreviousBalance();
    expect(superService.getPreviousBalance).toHaveBeenCalled();
  }));

  it('should unallow withdrawal', (() => {
    component.previousBalance = -1;
    component.amount = 300;
    component.withdraw();
  }));

  it('should continue withdrawal (previous balance < 0)', (() => {
    component.previousBalance = -1;
    component.amount = 10;
    component.withdraw();
  }));

  it('should continue withdrawal (previous balance > 0)', (() => {
    component.previousBalance = 1;
    component.withdraw();
  }));

  // it('should withdraw', (() => {
  //   component.connectedUser = TestMock.connectedUserMck;
  //   spyOn(superService, 'depositOrWithdraw').and.returnValue(of(TestMock.postResultMck));
  //   component.continueWithdraw();
  //   expect(superService.depositOrWithdraw).toHaveBeenCalled();
  // }));
});
