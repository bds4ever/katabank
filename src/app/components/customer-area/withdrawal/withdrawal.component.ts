import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBarService } from 'src/app/services/mat-snack-bar.service';
import { MyObservableService } from 'src/app/services/my-observable.service';
import { SuperService } from '../../../services/super.service';


@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class WithdrawalComponent implements OnInit {

  amount: number;
  previousBalance: number;
  availableWithdraw: number; // montant de retrait possible

  connectedUser: any;

  isBalanceReady: boolean = false;
  isOverdraftLimitReached: boolean = false;
  overdraftLimitReached: number = 250;

  constructor(private superService: SuperService,
    private matSnackBarService: MatSnackBarService) { }

  ngOnInit(): void {
    this.getConnectedUser();
    // récuperer le solde de la dernière opération
    this.getPreviousBalance();
  }

  /**
   * @name getPreviousBalance
   * @desc récuperer le solde de la dernière opération
   */
  async getPreviousBalance() {
    await this.superService.getPreviousBalance().then((resp: number) => {
      this.previousBalance = resp;
      this.isBalanceReady = true;
      return this.previousBalance;
    }, () => {
      const message = `Sorry, your balance is not available`;
      this.matSnackBarService.openSnackBar(message, '', 'notif-alert');
    })
  }

  /**
   * @name getConnectedUser
   * @desc get connecter user
   */
  async getConnectedUser() {
    await this.superService.getConnectedUser().then((res: any) => {
      this.connectedUser = res;
    })
  }

  /**
   * @name withdraw
   * @desc retirer de l'argent de son compte
   */
  async withdraw() {
    if (this.previousBalance < 0 || this.previousBalance === 0) {
      // availableWithdraw = montant de retrait possible
      this.availableWithdraw = 250 + this.previousBalance;
      // si montant de retrait > limite retrait dispo
      if (this.amount > this.availableWithdraw) {
        const message = `Sorry, you have reached your overdraft limit of 250€.
                          You could only withdraw` + ` ` + this.availableWithdraw + `€`
        this.matSnackBarService.openSnackBar(message, '', 'notif-alert');
      } else {
        this.continueWithdraw();
      }
    } else {
      this.continueWithdraw();
    }
  }

  /**
   * @name continueWithdraw
   * @desc continuer l'operation de retrait
   */
  continueWithdraw() {
    // nouvelle opération
    if (this.connectedUser) {
      const dataToSave = {
        'typeOperation': 'withdraw',
        'deposit': 0,
        'withdrawal': this.amount,
        'idUser': this.connectedUser.objectId,
        'balance': this.previousBalance - this.amount
      }

      // lancement de la sauvegarde
      this.superService.depositOrWithdraw(dataToSave).subscribe(() => {
        this.matSnackBarService.openSnackBar(`Success`, '', 'notif-success');
        // maj de la balance sur la page courante
        this.amount = 0;
        this.ngOnInit();
      }, error => {
        this.matSnackBarService.openSnackBar(error.error.error, '', 'notif-alert');
      })
    }
  }
}
