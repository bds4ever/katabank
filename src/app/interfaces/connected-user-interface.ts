export interface ConnectedUserInterface {
  ACL: {};
  createdAt: string;
  email: string;
  objectId: string;
  updatedAt: string;
  username: string;
}
