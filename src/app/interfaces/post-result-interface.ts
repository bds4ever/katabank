export interface PostResultInterface {
  createdAt: string;
  objectId: string
}
