export interface UserOperationsInterface {
    balance: number,
    createdAt: string,
    deposit: number,
    idUser: string,
    objectId: string,
    typeOperation: string,
    updatedAt: string,
    withdrawal: number
}
