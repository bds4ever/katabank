import { TestBed } from '@angular/core/testing';
import { AppRoutingModule } from '../app-routing.module';

import { AuthentificationGuardService } from './authentification-guard.service';
import Parse from 'parse';

describe('AuthentificationGuardService', () => {
  let service: AuthentificationGuardService;
  let backup: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppRoutingModule
      ]
    });
    service = TestBed.inject(AuthentificationGuardService);
    backup = Parse.initialize;
    Parse.initialize = jasmine.createSpy();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test canActivate', () => {
    service.canActivate();
  });
});
