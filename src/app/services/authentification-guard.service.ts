import { Injectable } from '@angular/core';
import { Parse } from 'parse';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationGuardService {
  isAuthenticated: boolean = false;

  constructor(private router: Router) {

  }
  /**
   * @name canActivate
   * @desc manage authorization
   */
  async canActivate() {
    let isUserAuthenticated = await Parse.User.current();

    if (!isUserAuthenticated) {
      this.router.navigate(['mybank/connexion']);
    } else {
      this.isAuthenticated = true;
    }

    return this.isAuthenticated;
  }
}
