import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSnackBarService } from './mat-snack-bar.service';

describe('MatSnackBarService', () => {
  let service: MatSnackBarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        BrowserAnimationsModule
      ],
      providers: [
        MatSnackBarService
      ]
    });

    service = TestBed.inject(MatSnackBarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open snackbar', () => {
    const message = 'hello world!';
    const action = '';
    service.openSnackBar(message, action, '');
  });
});
