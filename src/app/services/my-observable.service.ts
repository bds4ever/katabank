import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyObservableService {
  private item = new Subject<any>();
  dataSend$ = this.item.asObservable();
  constructor() { }

  /**
   * @name sendData
   * @param cible = ecran à actualiser
   * @param data = données à transmettre
   */
  sendData(cible: string, data: any) {
    data.cible = cible;
    this.item.next(data);
  }
}


