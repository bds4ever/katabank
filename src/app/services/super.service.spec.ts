import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SuperService } from './super.service';
import { TestMock } from '../testUclass/test-mock';

describe('SuperService', () => {
  let superService: SuperService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        HttpClientTestingModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
      providers:[
        SuperService
      ]
    });
    
    superService = TestBed.inject(SuperService);
    httpMock = TestBed.inject(HttpTestingController);

    superService.connectedUser = TestMock.connectedUserMck;
  });

  /*********************
   * test entries data *
   *********************/
  it('should be created', () => {
    expect(superService).toBeTruthy();
  });

  it('should test getPreviousBalance', () => {
    superService.getPreviousBalance();
  });

  it('should test getAllBankOperation', () => {
    superService.getAllBankOperation();
  });

  it('should test getDepositData', () => {
    superService.getDepositData('test');
  });

  it('should test depositOrWithdraw', () => {
    superService.depositOrWithdraw({});
  });

  it('should test updateBalance', () => {
    superService.updateBalance({}, 'test');
  });

  it('should test getLocalStorageValues', () => {
    superService.getLocalStorageValues();
  });

  it('should test connexion', () => {
    superService.connexion({});
  });

  it('should test getAllBankOperationByUser', () => {
    superService.getAllBankOperationByUser('test');
  });
});
