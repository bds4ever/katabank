import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Parse } from 'parse';

@Injectable({
  providedIn: 'root'
})
export class SuperService {

  connectedUser: any;
  optionsReq: any;
  allBankOperation: any;
  allBankOperationByUser: any;
  lastOperation: any;

  previousBalance: number;

  constructor(private http: HttpClient) {
    this.optionsReq = {
      headers: {
        'X-Parse-Application-Id': 'AhHj3drv4PHeoNgDDEESBZeqc3eKbtDhdYm21xrq',
        'X-Parse-REST-API-Key': 'PSriIM4TKbQCQTky4050i0Aqp1goB14UsfMMunhA'
      }
    }
  }


  /**
   * @name connexion
   * @desc connexion à l'espace client
   * @param data as login & password
   */
  connexion(data) {
    const url = `https://parseapi.back4app.com/login`;
    const optionsReq: any = {
      headers: {
        'X-Parse-Application-Id': 'AhHj3drv4PHeoNgDDEESBZeqc3eKbtDhdYm21xrq',
        'X-Parse-REST-API-Key': 'PSriIM4TKbQCQTky4050i0Aqp1goB14UsfMMunhA',
        'X-Parse-Revocable-Session': 1
      }
    }
    return this.http.post(url, data, optionsReq);
  }

  /**
   * @name depositOrWithdraw
   * @desc déposer ou retirer de l'argent de son compte
   * @param data
   */
  depositOrWithdraw(data) {
    const url = `https://parseapi.back4app.com/classes/bankOperation`;
    return this.http.post(url, data, this.optionsReq);
  }

  /**
   * @name getDepositData
   * @desc récupérer le résulat de cette sauvegarde pour mettre à jour le solde
   * @param depositId
   */
  getDepositData(depositId) {
    const url = `https://parseapi.back4app.com/classes/bankOperation/` + depositId;
    return this.http.get(url, this.optionsReq);
  }

  /**
   * @name getAllBankOperation
   * @desc récupérer l'ensemble des opérations realisées
   * @param depositId
   */
  getAllBankOperation() {
    const url = `https://parseapi.back4app.com/classes/bankOperation`;
    return this.http.get(url, this.optionsReq);
  }

  /**
   * @name getAllBankOperation
   * @desc récupérer l'ensemble des opérations realisées par le user loggé
   * @param idConnectedUser
   */
  getAllBankOperationByUser(idConnectedUser) {
    return new Promise(async (resolve) => {
      const promise = this.getAllBankOperation().toPromise();
      promise.then(res => {
        this.allBankOperation = res;
        // opérations du user loggé
        this.allBankOperationByUser = this.allBankOperation.results.filter(
          el => el.idUser === idConnectedUser
        );
        resolve(this.allBankOperationByUser);
      });
    })
  }

  /**
   * @name updateBalance
   * @desc mettre à jour le solde de l'utilisateur
   * @param updatedBalance
   * @param depositId
   */
  updateBalance(updatedBalance, depositId) {
    const url = `https://parseapi.back4app.com/classes/bankOperation/` + depositId;
    return this.http.put(url, updatedBalance, this.optionsReq);
  }

  /**
   * @name getLocalStorageValues
   * @desc recuperer les valeurs du local storage
   */
  getLocalStorageValues() {
    return JSON.parse(localStorage.getItem("Parse/AhHj3drv4PHeoNgDDEESBZeqc3eKbtDhdYm21xrq/currentUser"));
  }

  /**
  * @name getConnectedUser
  * @desc recuperation des donnees du user connecte
  */
  getConnectedUser() {
    return new Promise(async (resolve) => {
      const marqueurUtilisateurCo: any = await Parse.User.current();
      if (marqueurUtilisateurCo) {
        const url = `https://parseapi.back4app.com/users/` + marqueurUtilisateurCo.id;
        const promise = this.http.get(url, this.optionsReq).toPromise();
        promise.then((res: any) => {
          res.email = marqueurUtilisateurCo.getEmail();
          this.connectedUser = res;
          resolve(this.connectedUser);
        });
      } else {
        /***************************
         * aucun utilisateur loggé *
         ***************************/
        return null;
      }
    })
  }

  /**
   * @name deposit
   * @desc déposer de l'argent sur son compte
   */
  getPreviousBalance() {
    return new Promise((resolve) => {
      // récuperer la liste des opérations du user loggé pour en extraire le solde de la dernière opération
      this.getAllBankOperationByUser(this.connectedUser.objectId).then((allOperations: any) => {
        this.allBankOperationByUser = allOperations;
        // si aucune opération dispo
        if (this.allBankOperationByUser.length === 0) {
          this.previousBalance = 0;
        }
        // si 1 seule opération dispo
        if (this.allBankOperationByUser.length === 1) {
          this.lastOperation = this.allBankOperationByUser[0];
          this.previousBalance = this.lastOperation.balance;
        } else if (this.allBankOperationByUser.length > 1) {
          this.lastOperation = this.allBankOperationByUser[this.allBankOperationByUser.length - 1];
          this.previousBalance = this.lastOperation.balance;
        }
        resolve(this.previousBalance);
      })
    })
  }
}
