import { ConnectedUserInterface } from 'src/app/interfaces/connected-user-interface'
import { PostResultInterface } from '../interfaces/post-result-interface'

export class TestMock {
  public static connectedUserMck: ConnectedUserInterface = {
    ACL: {},
    createdAt: 'test',
    email: 'test',
    objectId: 'test',
    updatedAt: 'test',
    username: 'test'
  }

  public static postResultMck: PostResultInterface = {
    createdAt: "2022-02-05T15:10:38.988Z",
    objectId: 'test'
  }
}
